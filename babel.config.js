const defaultPresets = process.env.BABEL_ENV === 'es' ? [] : [
  [
    '@babel/preset-env',
    {
      modules: ['esm', 'production-umd'].includes(process.env.BABEL_ENV) ? false : 'commonjs',
    },
  ],
];

//const defaultAlias = {
//  '@yanfoo/rbac-a': './src'
//};

const productionPlugins = [
  // ...
];

module.exports = {
  presets: defaultPresets,
  plugins: [
    ['@babel/plugin-proposal-class-properties', { /*loose: true*/ }],
    ['@babel/plugin-proposal-object-rest-spread', { /*loose: true*/ }],
    '@babel/plugin-transform-runtime',
    // for IE 11 support
    '@babel/plugin-transform-object-assign',
  ],
  ignore: [/@babel[\\|/]runtime/], // Fix a Windows issue.
  env: {
    cjs: {
      plugins: productionPlugins.concat([ 
        ["@babel/plugin-transform-modules-commonjs", { /*loose: true*/ }]
      ]),
    },
    development: {
      plugins: [
        // ...
      ],
    },
    esm: {
      plugins: productionPlugins,
    },
    es: {
      plugins: productionPlugins,
    },
    production: {
      plugins: productionPlugins,
    },
    'production-umd': {
      plugins: productionPlugins,
    },
    test: {
      sourceMaps: 'both',
      plugins: [
        // ...
      ],
    },
  },
};