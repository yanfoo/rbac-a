/**
 * RBAC Attribute interface
 * 
 * Usage:
 * 
 *  class TimeOfDayRBACAttribute extends RBACAttribute {
 *    async check({ from, to }) {
 *      const currentTimeStamp = new Date();
 * 
 *      if (typeof from === 'string') {
 *        from = new Date(currentTimeStamp.toISOString().substr(0, 10) + " " + from);
 *      }
 *      if (typeof to === 'string') {
 *        to = new Date(currentTimeStamp.toISOString().substr(0, 10) + " " + from);
 *      }
 * 
 *      return (from <= currentTimeStamp) && (to >= currentTimeStamp);
 *    } 
 *  }
 * 
 *  const timeAttribute = new TimeOfDayRBACAttribute();
 * 
 *  const valid = await timeAttribute.check({ from: "8:00", to: "17:00" });
 * 
 * 
 */
class RBACAttribute {
   /**
    * Check that the provide options satisfy certain expectations
    * 
    * @param {Object} _options 
    * @return {Promise<Boolean>}
    */
   async check(_options) {
      throw new Error('Not implemented');
   }
}

RBACAttribute.fromCallback = callback => {
   class LambdaRBACAttribute extends RBACAttribute {
      async check(options) {
         return callback(options);
      }
   };

   return new LambdaRBACAttribute();
};

export default RBACAttribute;