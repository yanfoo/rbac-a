import merge from 'merge';

import { RBACValidationError } from './errors';
import RBACProvider from './provider';
import RBACAttribute from './attribute';

class RBAC {
   /**
   * Create a new instance of RBAC
   * 
   * @public
   * @param {Provider} provider                               will provide methods to return roles, permission and attributes
   * @param {Map<String,Function|RBACAttribute>} attributes   a object mapping attributes to a validation function
   * @param {Function} onError                                call this function whenever a validation error occurs. 
   *                                                          The function receives an Error augmented with the user and roles being validated
   * @param {Object} checkOptions                             default options, will be merged with the options argument of the check method
   * @param {Boolean} ignoreMissingAttributes                 do not revoke permission on missing attributes (default: false)
   */
  constructor({ provider, attributes, checkOptions, onError }) {
    if (!provider) {
      throw new Error('Missing provider');
    } else if (!(provider instanceof RBACProvider)) {
      throw new Error('Invalid RBACProvider instance');
    }

    if (!attributes) {
      attributes = new Map();
    } else if (!(attributes instanceof Map)) {
      if (typeof attributes === 'object') {
        attributes = new Map(Object.entries(attributes));
      } else {
        throw new Error('Invalid attributes');
      }
    }

    for (const [key,attribute] of attributes.entries()) {
      if (typeof attribute === 'function') {
        attributes.set(key, RBACAttribute.fromCallback(attribute));
      } else if (!(attribute instanceof RBACAttribute)) {
        throw new TypeError('Invalid attribute : ' + key);
      }
    }

    this.provider = provider;
    this.attributes = attributes;
    this.checkOptions = checkOptions;
  }

  /**
   * Check the access level of the given user for the specified permissions.
   * The returned valud may be a zero index-based number if all permissions
   * are satisfied, meaning that the value matches the greatest access level
   * found among the permissions, or the returned value may be NaN whenever
   * any permission fails to be satisfied.
   * 
   * @public
   * @param {Any} user                    a value representing the user to check
   * @param {String} permissions          some permissions to check
   * @param {Object} options              options passed to the provider when validating
   * @return {Promise<Number|NaN>}        resolve with the best access level found, or NaN
   */
  async check(user, permissions, options) {
    if (typeof permissions === 'string') {
      permissions = permissions.split(RBAC.PermissionListSeparator);
    } else if (permissions instanceof Set) {
      permissions = Array.from(permissions);
    } else if (!Array.isArray(permissions)) {
      throw new TypeError('Permissions must be a string, a set or an array, ' + permissions + ' given');
    }
    
    permissions = permissions.map(p => p.trim().toLocaleLowerCase()).filter(p => p);

    if (!permissions.length) {
      throw new Error('No permissions to check!');
    }

    const { 
      ignoreMissingAttributes,
      providerOptions,
      onError,
      onSuccess
    } = merge({ ...options, ...this.checkOptions });

    let userRoles;
    let accessRole = undefined;
    let accessLevel = NaN;  // default access is, virtually, no access
    
    try {
      
      // [ { role:string, lavel:number, permissions:[ { can:string, when:array<object> }, ... ] }, ... ]
      userRoles = await this.provider.getRoles(user, providerOptions);
      
      if (userRoles instanceof Set) {
        userRoles = Array.from(userRoles);
      } else if (!Array.isArray(userRoles)) {
        throw new TypeError('Provider value error, expected array or set, got : ' + userRoles);
      }
      
      // user roles cleanup and overhead
      userRoles = userRoles.map(({ role, level, permissions }) => {
        if (typeof permissions === 'string') {
          permissions = permissions.split(RBAC.PermissionListSeparator).map(can => ({ can }));
        } else if (permissions instanceof Set) {
          permissions = Array.from(permissions);
        } else if (!Array.isArray(permissions)) {
          throw new TypeError('Role permissions error, expected string, set or array, got : ' + permissions + ' for role ' + role);
        }
        level = level || 0;

        permissions = permissions.map(({ can, when }) => {
          if (typeof can === 'string') {
            can = can.split(RBAC.PermissionListSeparator);
          } else if (can instanceof Set) {
            can = Array.from(can);
          } else if (!Array.isArray(can)) {
            throw new TypeError('Invalid user role (permissions.can), see Provider.getRoles for valid return value');
          }

          can = can.map(p => p.trim().toLocaleLowerCase()).filter(p => p);

          if (when instanceof Map) {
            when = Object.fromEntries(when);
          } else if (when && (typeof when !== 'object')) {
            throw new TypeError('Invalid user role (permissions.when), see Provider.getRoles for valid return value');
          }
    
          return { can, when };
        }).filter(({ can }) => can?.length);

        return { role, level, permissions };
      }).filter(({ permissions }) => permissions.length).sort((a, b) => a.level - b.level);
      
      permissionCheckLoop:
      for (const permission of permissions) {
        for (const { role, level, permissions:rolePermissions } of userRoles) {
          rolePermissionCheckLoop:
          for (const { can, when } of rolePermissions) {
            // if this user role defines this permission
            
            if (can.some(p => p === permission)) {
              // if we have constraints....
              if (when) {
                for (const attributeName in when) {
                  const attribute = this.attributes.get(attributeName);

                  if (attribute) {
                    let validAttribute = await attribute.check(when[attributeName], user);

                    if (!validAttribute) {
                      continue rolePermissionCheckLoop;   // failed permission should skip to next rolePermission
                    }
                  } else if (!ignoreMissingAttributes) {
                    continue rolePermissionCheckLoop;      // missing attribute, 
                  }
                }
              }

              accessRole = role;
              // since we have sorted the userRoles' levels in ascending order
              accessLevel = level;  
              // we can break out of these nested loops, now, because
              // any more check would test for "lesser" access level
              break permissionCheckLoop;
            }
          } // rolePermissionCheckLoop
        }      
      } // permissionCheckLoop
    } catch (err) {
      onError && onError(new RBACValidationError(err, user, userRoles));
    }

    accessRole && onSuccess && onSuccess({ user, role:accessRole, level:accessLevel });

    return accessLevel;
  }
}

RBAC.PermissionListSeparator = ',';

export default RBAC;