export class RBACValidationError extends Error {
   constructor(error, user, roles) {
      super(error.message);

      this.error = error;
      this.user = user;
      this.roles = roles;
   }
}