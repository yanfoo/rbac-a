import { isIterable } from './utils';

/**
 * Provider interface
 */
class RBACProvider {
  /**
   * Returns a Promise resolving to all the roles associated with the specified user.
   * 
   * Usage:
   * 
   *  const userRoles = await provider.getRoles(2);
   *  [
   *    { role: 'editor', lavel: 0, permissions: [ { can: 'create' }, { can: 'edit' } ] },
   *    { role: 'contributor', label: 1, permissions: [ { can: 'edit', when: { timeInterval: { from: 8, to 17 } } } ] },
   *    { role: 'authenticatedUser', lavel: 2, permissions: [ { can: 'login' } ] }
   *  ]
   * 
   * 
   * NOTE : the property "level" is only used for hierarchical roles. If the value is omitted, it
   *        is considered to be 0.
   * 
   * @param {Any} user                           a value representing the user to check
   * @param {Object} options 
   * @return {Promise<Array<UserRole>>}
   */
  async getRoles(_user, _options) {
    throw new Error('Not implemented');
  }
}

/**
 * Provide roles from a JSON object
 * 
 * Usage:
 * 
 *  const permissionData = {
 *    roles: {
 *      editor: {
 *        permissions: [
 *          [ can: 'create' },
 *          { can: 'edit' }   // no restriction from attributes
 *        ],
 *        inherited: [ 'contributor' ]
 *      },
 *      contributor: {
 *        permissions: [
 *          { can: 'edit', when: { timeInterval: { from: 8, to 17 } } }
 *        ],
 *        inherited: [ 'authenticatedUser' ]
 *      }
 *      authenticatedUser: {
 *        permissions: [
 *          { can: 'login' }
 *        ]
 *      }
 *    },
 *    users: {
 *      1: ['contributor'],
 *      2: ['editor'],
 *      3: ['authenticatedUser']
 *    }
 *  };
 * 
 *    const provider = new JsonRBACProvider(permissionData);
 */
class JsonRBACProvider extends RBACProvider {
  /**
   * Construct a new JsonRBACProvider
   * 
   * @param {Object} options.users 
   * @param {Object} options.roles
   */
  constructor({ users, roles } = {}) {
    super();

    if (roles === null || typeof roles !== 'object') {
      throw new TypeError('Invalid roles in JsonRBACProvider');
    } else if (!(roles instanceof Map)) {
      roles = new Map(Object.entries(roles));
    }
    if (users === null || typeof users !== 'object') {
      throw new TypeError('Invalid users in JsonRBACProvider');
    } else if (!(users instanceof Map)) {
      users = new Map(Object.entries(users));
    }

    this.users = users;
    this.roles = roles;
  }

  /**
   * Retrive all the roles associated with the specified user. The returned value should be a map of the
   * roles associated with a list of sub-roles
   * 
   * @param {String} user                       a value representing the user to check
   * @return {Promise<Array<Object>>}
   */
  async getRoles(user) {
    const roles = this.roles;
    const collect = (collected, userRoles, level) => {
      const nextRoles = [];
      if (typeof userRoles === 'string' || !isIterable(userRoles)) {
        userRoles = [ userRoles ];
      }
      
      for (const userRole of userRoles) {
        if (roles.has(userRole)) {
          const role = roles.get(userRole);
          const permissions = (Array.isArray(role?.permissions) 
            ? role.permissions 
            : isIterable(role?.permissions) 
              ? Array.from(role.permissions) 
              : []).filter(p => p?.can?.length && (typeof p.can === 'string'));

          if (permissions?.length) {
            collected.push({ 
              role: userRole, 
              level,
              permissions
            });
          }

          if (typeof role?.inherited === 'string' && role.inherited.length) {
            nextRoles.push(role.inherited);
          } else if (role?.inherited instanceof Set) {
            nextRoles.push(...Array.from(role.inherited));
          } else if (role?.inherited?.length) {
            nextRoles.push(...role.inherited);
          }
        }
      }

      return nextRoles.length ? collect(collected, nextRoles, level + 1) : collected;
    };

    return collect([], this.users.get(String(user)) || [], 0);
  }
}

export default RBACProvider;
export { JsonRBACProvider };