/**
 * Check that the given argument is iterable
 * 
 * @param {Any} obj
 * @return {Boolean}
 */
export const isIterable = obj => obj && (typeof obj[Symbol.iterator] === 'function');