import RBACAttribute from '../src/attribute';



describe('Testing Attribute', () => {

   describe('Testing interface', () => {
 
     it('should throw not implemented', async () => {
 
       const attribute = new RBACAttribute();
 
       await expect(attribute.check()).rejects.toEqual(new Error('Not implemented'));
     });
 
   });


   describe('Testing from callback', () => {

      it('should check from callback', async () => {
         const attribute = RBACAttribute.fromCallback(() => true);

         await expect(attribute.check()).resolves.toBe(true);         
      });

   });
 
});