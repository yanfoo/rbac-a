import RBACProvider, { JsonRBACProvider } from '../src/provider';

describe('Testing Provider', () => {

  describe('Testing interface', () => {

    it('should throw not implemented', async () => {

      const provider = new RBACProvider();

      await expect(provider.getRoles()).rejects.toEqual(new Error('Not implemented'));
    });

  });




  describe('Testing JsonRBACProvider', () => {

    const users = {
      1: ['editor'],
      2: ['contributor'],
      3: ['authenticatedUser'],
      33: 'authenticatedUser',
      41: ['iterable'],
      42: ['missing'],
      43: ['nullPermissions'],
      50: new Set(['inheritThroughSet']),
      51: 'inheritThroughString'
    };
    const roles = {
      editor: {
        permissions: [
          { can: 'create' },
          { can: 'edit' }   // no restriction from attributes
        ],
        inherited: [ 'contributor' ]
      },
      contributor: {
        permissions: [
          { can: 'edit', when: { timeInterval: { from: "8:00", to: "17:00" } } }
        ],
        inherited: [ 'authenticatedUser' ]
      },
      authenticatedUser: {
        permissions: [
          { can: 'login' }
        ]
      },
      iterable: {
        permissions: new Set([ { can: 'iterate' } ])
      },
      nullPermissions: {},

      inherited: {
        permissions: [{ can:'exist' }]
      },
      inheritThroughSet: {
        permissions: [{ can:'inherit' }],
        inherited: new Set(['inherited'])
      },
      inheritThroughString: {
        permissions: [{ can:'inherit' }],
        inherited: 'inherited'
      }
    }

    const provider = new JsonRBACProvider({ users, roles });


    it('should return roles', async () => {
      const userRoles = await provider.getRoles(1);

      expect(userRoles).toHaveLength(3);
      expect(userRoles[0].role).toEqual('editor');
      expect(userRoles[1].role).toEqual('contributor');
      expect(userRoles[2].role).toEqual('authenticatedUser');
    });


    it('should return empty roles for non-existing users', async () => {
      const userRoles = await provider.getRoles(-1);

      expect(userRoles).toHaveLength(0);
    });

    
    it('should accept users with single role as string', async () => {
      const userRoles = await provider.getRoles(33);

      expect(userRoles).toHaveLength(1);
      expect(userRoles[0].role).toEqual('authenticatedUser');
    });


    it('should accept role permission as iterable', async () => {
      const userRoles = await provider.getRoles(41);

      expect(userRoles).toHaveLength(1);
      expect(userRoles[0].role).toEqual('iterable');
      expect(userRoles[0].permissions).toBeInstanceOf(Array);
    });


    it('should ignore missing role', async () => {
      const userRoles = await provider.getRoles(42);

      expect(userRoles).toHaveLength(0);
    });


    it('should ignore missing permissions', async () => {
      const userRoles = await provider.getRoles(43);

      expect(userRoles).toHaveLength(0);
    });


    it('should fail if no argument is provided', () => {
      expect(() => new JsonRBACProvider()).toThrow('Invalid roles in JsonRBACProvider');
    });


    it('should fail if roles is not an object', () => {
      const roles = null;
      const users = {};

      expect(() => new JsonRBACProvider({ users, roles })).toThrow('Invalid roles in JsonRBACProvider');
    });


    it('should fail if users is not an object', () => {
      const roles = {};
      const users = null;

      expect(() => new JsonRBACProvider({ users, roles })).toThrow('Invalid users in JsonRBACProvider');
    });


    it('should accept maps for roles and users', () => {
      const roles = new Map();
      const users = new Map();

      const provider = new JsonRBACProvider({ users, roles });

      expect(provider.users).toBe(users);
      expect(provider.roles).toBe(roles);
    });


    it('should inherit through Set', async () => {
      const provider = new JsonRBACProvider({ users, roles });

      const userRoles = await provider.getRoles(50);

      expect(userRoles).toHaveLength(2);
      expect(userRoles[0].role).toBe('inheritThroughSet');
      expect(userRoles[1].role).toBe('inherited');
    });


    it('should inherit through String', async () => {
      const provider = new JsonRBACProvider({ users, roles });

      const userRoles = await provider.getRoles(51);

      expect(userRoles).toHaveLength(2);
      expect(userRoles[0].role).toBe('inheritThroughString');
      expect(userRoles[1].role).toBe('inherited');
    });


  });

});