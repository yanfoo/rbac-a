import RBAC from '../src/rbac';
import RBACProvider from '../src/provider';
import RBACAttribute from '../src/attribute';


describe('Testing RBAC', () => {

  class MockProvider extends RBACProvider {
    async getRoles(_user, _options) {
      return [
        { role: 'dummy', level: 0, permissions: [{ can: 'test' }] },
        { role: 'numeric', level: 0, permissions: new Set([{ can: new Set(['number']), when: { min: { n: 2 } } }]) },
        { role: 'numeric', level: 1, permissions: [{ can: ['number'], when: new Map(Object.entries({ min: { n: 4 } })) }] },
        { role: 'multiAttr', level: 2, permissions: [{ can: 'multi', when: { foo: true, bar: false } }] }
      ];
    }  
  }

  const mockProvider = new MockProvider();

  describe('Testing constructor', () => {

    it('should fail if no argument supplied', () => {
      expect(() => new RBAC()).toThrow("Cannot read property 'provider' of undefined");
    });

    it('should fail if no provider supplied', () => {
      expect(() => new RBAC({})).toThrow('Missing provider');
    });

    it('should fail if invalid provider', () => {
      expect(() => new RBAC({ provider: true })).toThrow('Invalid RBACProvider instance');
    })

    it('should fail with invalid attributes', () => {
      expect(() => new RBAC({ provider:mockProvider, attributes:true })).toThrow('Invalid attributes');
    });

    it('should map attributes to RBACAttribute', () => {
      const objAttributes = {
        foo: () => true
      };
      const mapAttributes = new Map(Object.entries(objAttributes));

      const rbac1 = new RBAC({ provider:mockProvider, attributes:objAttributes });
      const rbac2 = new RBAC({ provider:mockProvider, attributes:mapAttributes });

      expect(rbac1.attributes).toBeInstanceOf(Map);
      expect(rbac1.attributes.has('foo')).toBe(true);

      expect(rbac2.attributes).toBeInstanceOf(Map);
      expect(rbac2.attributes.has('foo')).toBe(true);
    });

    it('should defaults to empty attributes map', () => {
      const rbac = new RBAC({ provider:mockProvider });

      expect(rbac.attributes.size).toBe(0);
    });

    it('should fail with invalid attribute', () => {
      const attributes = {
        foo: true
      };
      expect(() => new RBAC({ provider:mockProvider, attributes })).toThrow('Invalid attribute : foo');
    });

  });


  describe('Testing check', () => {

    it('should fail to check if invalid userRoles', async () => {
      class DummyProvider extends RBACProvider {
        async getRoles(_user, _options) {
          return {};
        }  
      }

      const provider = new DummyProvider();

      const rbac = new RBAC({ 
        provider,
        checkOptions: {
          onError: err => {
            errorThrown = true;

            expect(err.message).toBe('Provider value error, expected array or set, got : [object Object]');
          }
        }
      });
      let errorThrown = false;

      await expect(rbac.check(1, 'invalid')).resolves.toBeNaN();

      expect(errorThrown).toBe(true);
    });
    

    it('should fail to check if invalid permissions', async () => {
      const rbac = new RBAC({ provider:mockProvider });

      expect(rbac.check(1, null)).rejects.toThrow('Permissions must be a string, a set or an array, null given');
      expect(rbac.check(1, 1)).rejects.toThrow('Permissions must be a string, a set or an array, 1 given');
      expect(rbac.check(1, new Map)).rejects.toThrow('Permissions must be a string, a set or an array, [object Map] given');

      expect(rbac.check(1, [])).rejects.toThrow('No permissions to check!');
      expect(rbac.check(1, new Set())).rejects.toThrow('No permissions to check!');
      expect(rbac.check(1, '')).rejects.toThrow('No permissions to check!');
    });


    it('should fail for invalid user role permissions', async () => {
      class DummyProvider extends RBACProvider {
        async getRoles(_user, _options) {
          return [
            { role: 'invalid', level: 0, permissions: null },
          ];
        }  
      }

      const provider = new DummyProvider();

      const rbac = new RBAC({ provider });
      let errorThrown = false;

      await expect(rbac.check(1, 'invalid', {
        onError: err => {
          errorThrown = true;

          expect(err.message).toBe('Role permissions error, expected string, set or array, got : null for role invalid');
        }
      })).resolves.toBeNaN();

      expect(errorThrown).toBe(true);
    });


    it('should fail if invalid user role permission "can"', async () => {
      class DummyProvider extends RBACProvider {
        async getRoles(_user, _options) {
          return [
            { role: 'invalid', level: 0, permissions: [{ can: {} }] },
          ];
        }  
      }

      const provider = new DummyProvider();

      const rbac = new RBAC({ provider });
      let errorThrown = false;

      await expect(rbac.check(1, 'invalid', {
        onError: err => {
          errorThrown = true;

          expect(err.message).toBe('Invalid user role (permissions.can), see Provider.getRoles for valid return value');
        }
      })).resolves.toBeNaN();

      expect(errorThrown).toBe(true);
    });


    it('should fail if invalid user role permission "when"', async () => {
      class DummyProvider extends RBACProvider {
        async getRoles(_user, _options) {
          return [
            { role: 'invalid', level: 0, permissions: [{ can: 'invalid', when: true }] },
          ];
        }  
      }

      const provider = new DummyProvider();

      const rbac = new RBAC({ 
        provider,
        checkOptions: {
          onError: err => {
            errorThrown = true;

            expect(err.message).toBe('Invalid user role (permissions.when), see Provider.getRoles for valid return value');
          }
        }
      });
      let errorThrown = false;

      await expect(rbac.check(1, 'invalid')).resolves.toBeNaN();

      expect(errorThrown).toBe(true);
    });


    it('should swallow errors and return NaN', async () => {
      class DummyProvider extends RBACProvider {
        async getRoles(_user, _options) {
          return {};
        }  
      }

      const provider = new DummyProvider();

      const rbac = new RBAC({ provider });

      await expect(rbac.check(1, 'invalid')).resolves.toBeNaN();
    });


    it('should check permission', async () => {
      const rbac = new RBAC({ provider:mockProvider });

      await expect(rbac.check(1, 'test')).resolves.toBe(0);
    });


    it('should check userRoles as Set', async () => {
      class DummyProvider extends RBACProvider {
        async getRoles(_user, _options) {
          return new Set([
            { role: 'test', level: 4, permissions: 'test' },
          ]);
        }  
      }

      const provider = new DummyProvider();
      const rbac = new RBAC({ provider });
      let foundUser, foundRole, foundLevel;

      await expect(rbac.check(1, 'test', {
        onSuccess: ({ user, role, level }) => {
          foundUser = user;
          foundRole = role;
          foundLevel = level;
        }
      })).resolves.toBe(4);

      expect(foundUser).toBe(1);
      expect(foundRole).toBe('test');
      expect(foundLevel).toBe(4);
    });


    it('should check with attribute', async () => {
      const attributes1 = {
        min: ({ n }) => n >= 2
      }
      const attributes2 = {
        min: RBACAttribute.fromCallback(({ n }) => n >= 4)
      };

      const rbac1 = new RBAC({ provider:mockProvider, attributes:attributes1 });
      const rbac2 = new RBAC({ provider:mockProvider, attributes:attributes2 });


      await expect(rbac1.check(1, 'number')).resolves.toEqual(0);
      await expect(rbac2.check(1, 'number')).resolves.toEqual(1);
    });


    it('should check fail with missing attributes', async () => {
      const attributes = {
        foo: t => t,
        // bar is missing
      };

      const rbac = new RBAC({ provider:mockProvider, attributes });

      await expect(rbac.check(1, 'multi')).resolves.toBeNaN();
    });


    it('should check success with missing attributes', async () => {
      const attributes = {
        foo: t => t,
        // bar is missing
      };

      const rbac = new RBAC({ 
        provider:mockProvider, 
        attributes,
        checkOptions: { ignoreMissingAttributes: true } 
      });

      await expect(rbac.check(1, 'multi')).resolves.toBe(2);
    });

  });


  describe('Testing permission override', () => {

    class MockProvider extends RBACProvider {
      async getRoles(user, _options) {
        if (user === 1) {
          return [
            { role:'test', level:0, permissions: [{ can:['test_1', 'test_2'] }] },
            { role:'test', level:1, permissions: [{ can:'test_2', when: { restrict: true } }] },
          ];
        } else {
          return [
            { role:'test', level:0, permissions: [{ can:'test_2', when: { restrict: true } }] },
          ];
        }
      }  
    }

    const provider = new MockProvider();
    const attributes = {
      restrict: () => false
    };
  
    it('should restrict the second user only', async () => {
      const rbac = new RBAC({ provider, attributes });

      await expect(rbac.check(1, 'test_2')).resolves.toBe(0);
      await expect(rbac.check(2, 'test_2')).resolves.toBeNaN();

    });


  });


});