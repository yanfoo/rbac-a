import { RBACValidationError } from '../src/errors';



describe('Testing errors', () => {

   it('should expose RBACValidationError', () => {
      expect( RBACValidationError ).not.toBeUndefined();
      expect( new RBACValidationError(new Error('Test')) ).toBeInstanceOf(Error);
      expect( new RBACValidationError(new Error('Test')).error ).toBeInstanceOf(Error);
      expect( new RBACValidationError(new Error('test'), 'testUser').user ).toEqual('testUser');
      expect( new RBACValidationError(new Error('test'), 'testUser', 'testRoles').roles ).toEqual('testRoles');
   });

});